This is a docker image for running TML on the web.

See https://github.com/idni/tau for more information about TML and Tauchain.

Live example should be running at: https://tml.klapka.cz/
